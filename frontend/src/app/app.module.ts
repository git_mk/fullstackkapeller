import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CreateKundeComponent } from './create-kunde/create-kunde.component';
import { KundeDetailsComponent } from './kunde-details/kunde-details.component';
import { KundeListComponent } from './kunde-list/kunde-list.component';
import { UpdateKundeComponent } from './update-kunde/update-kunde.component';
import {AppRoutingModule} from "./routing.module";
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    CreateKundeComponent,
    KundeDetailsComponent,
    KundeListComponent,
    UpdateKundeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
