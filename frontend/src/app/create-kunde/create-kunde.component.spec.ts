import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateKundeComponent } from './create-kunde.component';

describe('CreateKundeComponent', () => {
  let component: CreateKundeComponent;
  let fixture: ComponentFixture<CreateKundeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateKundeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateKundeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
