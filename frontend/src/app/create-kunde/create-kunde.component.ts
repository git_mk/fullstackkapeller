import { Component, OnInit } from '@angular/core';
import {Kunde} from "../../model/kunde";
import {KundeService} from "../kunde.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-kunde',
  templateUrl: './create-kunde.component.html',
  styleUrls: ['./create-kunde.component.css']
})
export class CreateKundeComponent implements OnInit {

  kunde:Kunde = new Kunde();
  submitted = false;

  constructor(private kundeService: KundeService, private router: Router) { }

  ngOnInit(): void {
  }
  newKunde(): void {
    this.submitted= false;
    this.kunde = new Kunde();
  }
  save(){
    this.kundeService.createKunde(this.kunde).subscribe(
      data => { console.log(data)
      this.kunde = new Kunde();
      this.gotoList();
      }
    )
  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }
  gotoList(){
    this.router.navigate(['/kunde']);
  }
}
