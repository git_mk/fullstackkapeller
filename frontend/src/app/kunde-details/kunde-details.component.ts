import { Component, OnInit } from '@angular/core';
import {Kunde} from "../../model/kunde";
import {KundeService} from "../kunde.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-kunde-details',
  templateUrl: './kunde-details.component.html',
  styleUrls: ['./kunde-details.component.css']
})
export class KundeDetailsComponent implements OnInit {
  id:number;
  kunde: Kunde;

  constructor(private route: ActivatedRoute,private router: Router,
              private kundeService:KundeService) { }

  ngOnInit(): void {
    this.kunde = new Kunde();

    this.id=this.route.snapshot.params['id'];
    this.kundeService.getKunde(this.id)
      .subscribe(
        data => {
          console.log(data)
          this.kunde = data;
        });

    }
    list(){
    this.router.navigate(['kunde']);
    }
  }


