import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import { Kunde } from "../../model/kunde"
import {KundeService} from "../kunde.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-kunde-list',
  templateUrl: './kunde-list.component.html',
  styleUrls: ['./kunde-list.component.css']
})
export class KundeListComponent implements OnInit {
  kunden:Observable<Kunde[]>;

  constructor(private kundeService:KundeService, private router: Router){ }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData(){
    this.kunden = this.kundeService.getKundeList();
  }
  deleteKunde(id:number){
    this.kundeService.deleteKunde(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        }
      );
  }
  kundeDetails(id:number){
    this.router.navigate(['details', id]);
  }
  updateKunde(id:number){
    this.router.navigate(['update', id]);
  }

}
