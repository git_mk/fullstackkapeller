import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Kunde} from "../model/kunde";

@Injectable({
  providedIn: 'root'
})
export class KundeService {
  private url = 'http://localhost:8080/kunde';

  constructor(private http:HttpClient) { }
  getKunde(id:number):Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }
  createKunde(kunde: Object):Observable<Object> {
    return this.http.post(`${this.url}`,kunde)
  }
  updateKunde(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.url}/${id}`, value);
  }
  deleteKunde(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`, { responseType: 'text' });
  }
  getKundeList(): Observable<any> {
    return this.http.get(`${this.url}`);
  }

}
