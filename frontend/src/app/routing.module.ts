import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {KundeListComponent} from "./kunde-list/kunde-list.component";
import {CreateKundeComponent} from "./create-kunde/create-kunde.component";
import {UpdateKundeComponent} from "./update-kunde/update-kunde.component";
import {KundeDetailsComponent} from "./kunde-details/kunde-details.component";


const routes: Routes = [
  { path: '', redirectTo: 'employee', pathMatch: 'full' },
  { path: 'kunde', component: KundeListComponent },
  { path: 'add', component: CreateKundeComponent },
  { path: 'update/:id', component: UpdateKundeComponent },
  { path: 'details/:id', component: KundeDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
