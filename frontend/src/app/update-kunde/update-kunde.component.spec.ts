import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateKundeComponent } from './update-kunde.component';

describe('UpdateKundeComponent', () => {
  let component: UpdateKundeComponent;
  let fixture: ComponentFixture<UpdateKundeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateKundeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateKundeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
