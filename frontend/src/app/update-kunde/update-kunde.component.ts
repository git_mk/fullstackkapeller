import { Component, OnInit } from '@angular/core';
import {Kunde} from "../../model/kunde";
import {KundeService} from "../kunde.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-update-kunde',
  templateUrl: './update-kunde.component.html',
  styleUrls: ['./update-kunde.component.css']
})
export class UpdateKundeComponent implements OnInit {
  id: number;
  kunde: Kunde;
  submitted: false;

  constructor(private route: ActivatedRoute, private router: Router,
              private kundeService: KundeService) {
  }

  ngOnInit(): void {
    this.kunde = new Kunde();
    this.id = this.route.snapshot.params['id'];
    this.kundeService.getKunde(this.id)
      .subscribe(
        data => {
          console.log(data)
          this.kunde = data;
        });
  }

  updateKunde() {
    this.kundeService.updateKunde(this.id, this.kunde)
      .subscribe(
        data => {
          console.log(data);
          this.kunde = new Kunde();
          this.gotoList();
        });
  }
  onSubmit(){
    this.updateKunde();
  }
  gotoList(){
    this.router.navigate(['/kunde']);
  }
}
