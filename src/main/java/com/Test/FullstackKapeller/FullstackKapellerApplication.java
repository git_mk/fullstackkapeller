package com.Test.FullstackKapeller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullstackKapellerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FullstackKapellerApplication.class, args);
	}

}
