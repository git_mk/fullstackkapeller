package com.Test.FullstackKapeller.controller;

import com.Test.FullstackKapeller.domain.Kunde;
import com.Test.FullstackKapeller.exceptions.ResourceNotFoundException;
import com.Test.FullstackKapeller.service.KundeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class KundeController {
    @Autowired
    KundeService kundeService;
    private final String kundenPath = "/kunde";

    @GetMapping(kundenPath)
    public List<Kunde> getAllKunden() {
        if(kundeService.getAll().isEmpty())
            throw  new ResourceNotFoundException("Keine Kunden gefunden");
        return kundeService.getAll();
    }

    @GetMapping(kundenPath + "/{kid}")
    public ResponseEntity<Kunde> getKundebyId(@PathVariable Long kid) {
        Kunde kunde=kundeService.get(kid);
        return ResponseEntity.ok().body(kunde);
    }

    @DeleteMapping(kundenPath + "/{kid}")
    public Map<String,Boolean> deleteKunde(@PathVariable Long kid) {
        kundeService.delete(kid);
        Map<String ,Boolean> response= new HashMap<>();
        response.put("deleted",Boolean.TRUE);
        return response;
    }

    @PutMapping(kundenPath + "/{kid}")
    public ResponseEntity<Kunde> updateKunde(@PathVariable Long kid, @Valid @RequestBody Kunde kunde) {
        kundeService.update(kid,kunde);
        return ResponseEntity.ok(kundeService.get(kid));
    }
    @PostMapping(kundenPath)
    public Kunde addKunde(@Valid@RequestBody Kunde kunde){
        if(!kundeService.businessLogik2checkIfNameIsDouble(kunde))
            throw new IllegalArgumentException("Name doppelt");
        return kundeService.save(kunde);
    }
}

