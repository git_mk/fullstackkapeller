package com.Test.FullstackKapeller.controller;

import com.Test.FullstackKapeller.domain.Kunde;
import com.Test.FullstackKapeller.domain.Rechnung;
import com.Test.FullstackKapeller.exceptions.ResourceNotFoundException;
import com.Test.FullstackKapeller.service.KundeService;
import com.Test.FullstackKapeller.service.RechnungService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class RechnungController {
    @Autowired
    RechnungService rechnungService;
    @Autowired
    KundeService kundeService;
    private final String rechnungsPath="/rechnung";

    @GetMapping(rechnungsPath)
    public List<Rechnung> getAllRechnungen(){
        if(rechnungService.getAll().isEmpty())
            throw new ResourceNotFoundException("Keine Rechnungen");
        return rechnungService.getAll();
    }
    @GetMapping({rechnungsPath+"/{rid}","kunde/{kid}/rechnung/{rid}"})
    public Rechnung getRechnung(@PathVariable Long rid){
        return rechnungService.get(rid);
    }
    @DeleteMapping({rechnungsPath+"/{rid}","kunde/{kid}/rechnung/{rid}"})
    public ResponseEntity<String> deleteRechnung(@PathVariable Long rid){
        rechnungService.delete(rid);
        return ResponseEntity.ok("gelöscht");
    }
    @PutMapping(rechnungsPath+"/{rid}")
    public Rechnung updateRechung(@PathVariable Long rid, @Valid@RequestBody Rechnung rechnung){
        return rechnungService.update(rid,rechnung);
    }
    @PostMapping("kunde/{kid}/rechnung")
    public Rechnung createRechnung(@PathVariable Long kid, @Valid@RequestBody Rechnung rechnung){
        Kunde kunde=kundeService.get(kid);
        return rechnungService.save(rechnung,kunde);
    }
    @GetMapping("kunde/{kid}/rechnung")
    public List<Rechnung> getAllRechnungFromKunde(@PathVariable Long kid){
        if(rechnungService.businessLogik1getRechnungFromKunde(kid).isEmpty())
            throw new ResourceNotFoundException("Keine Rechnungen unter diesen Kunden");
        return rechnungService.businessLogik1getRechnungFromKunde(kid);
    }
}
