package com.Test.FullstackKapeller.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="kunde")
public class Kunde {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long kid;
    @NotNull
    private String kname;
    @NotNull
    private String beruf;

    public Long getKid() {
        return kid;
    }

    public void setKid(Long kid) {
        this.kid = kid;
    }

    public String getKname() {
        return kname;
    }

    public void setKname(String kname) {
        this.kname = kname;
    }

    public String getBeruf() {
        return beruf;
    }

    public void setBeruf(String beruf) {
        this.beruf = beruf;
    }
}
