package com.Test.FullstackKapeller.repo;

import com.Test.FullstackKapeller.domain.Kunde;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KundenRepo extends JpaRepository<Kunde,Long> {
}
