package com.Test.FullstackKapeller.repo;

import com.Test.FullstackKapeller.domain.Rechnung;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RechnungRepo extends JpaRepository<Rechnung,Long> {
}
