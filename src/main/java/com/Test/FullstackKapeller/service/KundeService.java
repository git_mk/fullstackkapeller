package com.Test.FullstackKapeller.service;

import com.Test.FullstackKapeller.domain.Kunde;
import com.Test.FullstackKapeller.repo.KundenRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KundeService {
    @Autowired
    KundenRepo kundenRepo;

    public List<Kunde> getAll(){
        return kundenRepo.findAll();
    }
    public Kunde save(Kunde kunde){
        return kundenRepo.save(kunde);
    }
    public Kunde get(Long kid){
        return kundenRepo.findById(kid).get();
    }
    public void delete(Long kid){
        kundenRepo.deleteById(kid);
    }
    public Kunde update(Long kid, Kunde kunde){
        Kunde alterKunde=kundenRepo.findById(kid).get();
        alterKunde.setBeruf(kunde.getBeruf());
        alterKunde.setKname(kunde.getKname());
        return kundenRepo.save(alterKunde);
    }
    public boolean businessLogik2checkIfNameIsDouble(Kunde kunde){
        List<Kunde> kList=kundenRepo.findAll();
        for(Kunde kunde1:kList){
            if(kunde1.getKname().equals(kunde.getKname()))
                return false;}
        return true;
    }
}
