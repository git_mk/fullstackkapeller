package com.Test.FullstackKapeller.service;

import com.Test.FullstackKapeller.domain.Kunde;
import com.Test.FullstackKapeller.domain.Rechnung;
import com.Test.FullstackKapeller.repo.RechnungRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RechnungService {
    @Autowired
    RechnungRepo rechnungRepo;

    public List<Rechnung> getAll(){
        return rechnungRepo.findAll();
    }
    public Rechnung get(Long rid){
        return rechnungRepo.findById(rid).get();
    }
    public Rechnung save(Rechnung rechnung, Kunde kunde){
        Rechnung rechnung1=new Rechnung();
        rechnung1.setKunde(kunde);
        rechnung1.setRname(rechnung.getRname());
        return rechnungRepo.save(rechnung1);
    }
    public void delete(Long rid){
        rechnungRepo.deleteById(rid);
    }
    public Rechnung update(Long rid,Rechnung rechnung){
        Rechnung alteRechnung=rechnungRepo.findById(rid).get();
        alteRechnung.setRname(rechnung.getRname());
        alteRechnung.setKunde(rechnung.getKunde());
        return rechnungRepo.save(alteRechnung);
    }
    public List<Rechnung> businessLogik1getRechnungFromKunde(Long kid){
        List<Rechnung> rList=rechnungRepo.findAll();
        List<Rechnung> rechnungList=new ArrayList<>();
        for(Rechnung rechnung:rList){
            if(rechnung.getKunde().getKid().equals(kid))
                rechnungList.add(rechnung);
        }
        return rechnungList;
    }

}
